#include "tspm.hpp"

using namespace std;

int K=0;                        //Maior distância não-infinita existente
int P = 0;                      //Número de facilidades a serem instaladas

int facNumber, custNumber;      //Número de facilidades; Número de clientes

vector<customer> customers;     //vetor de clientes
vector<facility> facilities;    //vetor de facilidades

vector<int> bestSolution;       //vetor solução final
vector<int> solution;           //vetor solução corrente

vector<int> tabuList;           //lsita tabu

int maxRuns = 15;
int maxDepth = 10;
int swapModifier = 4;
float tabuSizeFactor = 0.75;
int tabuListSize = 0;
int iterationCount = 0;
int swapCount = 0;
int currentCost = 0;
int bestCost = 0;
int iterationDepth = 0;
int tmlim=0;

int opFile = 1;


void printSolution()
{
    printf("\nSolucao Final: \n");
    int solSize = solution.size();
    for(int i=0; i<solSize; i++)
    {
        printf("%i, ", solution[i]+1);
    }
    printf("\n\n");
}

/*
Retorna o maior valor entre dois números
*/
int max(int n1, int n2)
{
    if(n1 > n2)
        return n1;
    else
        return n2;
}

/*
Retorna o menor valor entre dois números
*/
int min(int n1, int n2)
{
    if(n1 < n2)
        return n1;
    else
        return n2;
}

/*
Compara duas tuplas de distância. Verdadeiro se a primeira é menor que a segunda.
*/
bool compDistances (fdTuple tOne, fdTuple tTwo)
{
    return (tOne.distance < tTwo.distance);
}

/*
Verifica se uma facilidade está aberta
*/
bool isFacilityOpened(int facD)
{
    return facilities[facD].isOpened;
}

/*
Verifica se uma facilidade está na lista Tabu
*/
bool isFacilityTabu(int facD)
{
    int actTabuSize = tabuList.size();
    for(int i=0; i<actTabuSize; i++)
    {
        if(facD == tabuList[i])
            return true;
    }

    return false;
}

/*
Obtém a facilidade mais próxima ao cliente C e que está aberta
*/
fdTuple getCloserOpenedFacility(customer C, int startIndex = 0)
{
    int index = 0, flag=0;

    /*
        A flag funciona para que se possa também obter a segunda, ou n-ésima, facilidade
        mais próxima
    */
    if(startIndex < 0 )
        flag=0;
    else
        flag=startIndex;

    while(index < facNumber)
    {
        if(isFacilityOpened(C.orderedFacilityDistances[index].facility))
        {
            if(flag)
                flag--;
            else
            {
                return C.orderedFacilityDistances[index];
            }

        }
        index++;
    }

    //Se chegou aqui, não há nenhuma facilidade aberta próxima ao cliente
    return {-1,-1};
}

/*
Função para realizar a leitura dos arquivos GAP
*/
void gapFiles(FILE *inputFile)
{

    int facCounter, custCounter, facD, custD, distD;
    char ps[100];

    fgets(ps, 100, inputFile);
    fgets(ps, 100, inputFile);
    fscanf(inputFile, "%d %d", &facNumber, &P);
    fgets(ps, 100, inputFile);
    fgets(ps, 100, inputFile);

    custNumber = facNumber;

    facility fAux;
    customer cAux;

    facCounter = 0;
    custCounter = 0;

    vector<fdTuple> initFdistances;

    for(int i=0; i<facNumber; i++)
        initFdistances.push_back({i, INFINITY});

    while(facCounter < facNumber)
    {
        fAux.ID = facCounter+1;
        fAux.freq = 0;
        fAux.isOpened = false;
        facilities.push_back(fAux);

        cAux.ID = custCounter+1;
        cAux.currentFacility = -1;
        cAux.nextFacility = -1;
        cAux.facilityDistances=initFdistances;

        customers.push_back(cAux);

        facCounter++;
        custCounter++;
    }


    while(!feof(inputFile))
    {
        fscanf(inputFile, "\t%d\t%d\t%d\n", &facD, &custD, &distD);

        if(K < distD && distD < INFINITY)
            K=distD;

        customers[custD-1].facilityDistances[facD-1]= {facD-1, distD};
    }

    //Gera a lista ordenada por proximidade de cada cliente
    for(int i=0; i<custNumber; i++)
    {
        customers[i].orderedFacilityDistances = customers[i].facilityDistances;

        sort(customers[i].orderedFacilityDistances.begin(),customers[i].orderedFacilityDistances.end() , compDistances);
    }
}

/*
Função para realizar a leitura dos arquivos pMed
*/
void pmedFiles(FILE *inputFile)
{
    int facD, custD, distD, i;
    int facCounter, custCounter;

    fscanf(inputFile, "%d %d %d", &facNumber, &i, &P); // i é descartável
    custNumber = facNumber;

    facility fAux;
    customer cAux;

    facCounter = 0;
    custCounter = 0;

    vector<fdTuple> initFdistances;

    for(int i=0; i<facNumber; i++)
        initFdistances.push_back({i, INFINITY});

    while(facCounter < facNumber)
    {
        fAux.ID = facCounter+1;
        fAux.freq = 0;
        fAux.isOpened = false;
        facilities.push_back(fAux);

        cAux.ID = custCounter+1;
        cAux.currentFacility = -1;
        cAux.nextFacility = -1;
        cAux.facilityDistances=initFdistances;

        //Distância de si mesmo é 0, i.e., c[i][i] = 0
        cAux.facilityDistances[custCounter]= {custCounter, 0};

        customers.push_back(cAux);

        facCounter++;
        custCounter++;
    }


    while(!feof(inputFile))
    {
        fscanf(inputFile, "%d %d %d", &facD, &custD, &distD);

        if(feof(inputFile))
            break;

        if(K < distD && distD < INFINITY)
            K=distD;

        //Matriz de distâncias é simétrica: c[i][j] = c[j][i]
        customers[custD-1].facilityDistances[facD-1]= {facD-1, distD};
        customers[facD-1].facilityDistances[custD-1]= {custD-1, distD};
    }

    //Aplicação do algoritmo de Floyd para o cálculo das (menores) distâncias entre todos os vértices
    for(int i=0; i<custNumber; i++)
    {
        for(int j=0; j<custNumber; j++)
        {
            for(int k=0; k<custNumber; k++)
            {
                if(customers[i].facilityDistances[j].distance > customers[i].facilityDistances[k].distance+customers[k].facilityDistances[j].distance)
                    customers[i].facilityDistances[j].distance = customers[i].facilityDistances[k].distance+customers[k].facilityDistances[j].distance;
            }
            if(K < customers[i].facilityDistances[j].distance)
                K=customers[i].facilityDistances[j].distance;
        }
    }

    //Gera a lista ordenada por proximidade de cada cliente
    for(int i=0; i<custNumber; i++)
    {
        customers[i].orderedFacilityDistances = customers[i].facilityDistances;

        sort(customers[i].orderedFacilityDistances.begin(),customers[i].orderedFacilityDistances.end() , compDistances);
    }

}

void printCustomer(customer C)
{
    printf("ID: %i\nCF: %i\nNF: %i\n", C.ID, C.currentFacility, C.nextFacility);

    for(int i=0; i<facNumber; i++)
    {
        printf("( %i ,  %i )   ", C.orderedFacilityDistances[i].facility, C.orderedFacilityDistances[i].distance);
        if(isFacilityOpened(C.orderedFacilityDistances[i].facility))
            printf("<< opened >>\n");
        else
            printf("<< closed >>\n");
        if(C.orderedFacilityDistances[i].distance == INFINITY)
            break;
    }

}

/*
Retorna o custo de instalação de uma facilidade.
O número de vezes que ela já apareceu na solução (freq) influi neste custo para mais.
*/
int getSetUpCost(int f)
{
    int m;
    if(opFile == 1)
        m=0;
    else
        m=1;
    return K*facilities[f].freq*m;
}

/*
Gera uma possível solução inicial
*/
void generateInitialSolution()
{
    /*
    Distribui igualmente as facilidades na solução inicial.
    cada m/p facilidade é inserida na solução inicial.
    */
    int increment = facNumber/P;
    for(int i=0, j=0; j<P; i+=increment, j++)
    {
        solution.push_back(i);
        addToTabuList(i);
        facilities[i].isOpened = true; //Se a facilidade está na solução, ela deve estar aberta
    }

    checkTabuListSize();

    /*
    Realiza as ligações entre clientes as facilidades da solução inicial.
    */
    for(int i=0; i<custNumber; i++)
    {
        fdTuple closer = getCloserOpenedFacility(customers[i]);
        fdTuple secondCloser = getCloserOpenedFacility(customers[i], 1);

        customers[i].currentFacility = closer.facility;
        customers[i].nextFacility = secondCloser.facility;

        /*
        Atualiza as listas de conexões proximas para as facilidades
        */
        if(closer.facility >= 0)
            facilities[closer.facility].closestConnections.push_back(i);
        if(secondCloser.facility >= 0)
            facilities[secondCloser.facility].nextClosestConnections.push_back(i);
    }


}

bool isFactibleSolution()
{
    for(int i=0; i<custNumber; i++)
    {
        int f = customers[i].currentFacility;

        if(f < 0)   //Solução infactível, todos os clientes devem ser supridos!
            return false;
    }
    return true;
}

/*
Calcula o custo total da solução corrente
*/
int totalCost()
{
    int sum=0;
    for(int i=0; i<custNumber; i++)
    {
        int f = customers[i].currentFacility;

        if(f < 0)   //Solução infactível, todos os clientes devem ser supridos!
            return -INFINITY;

        sum += customers[i].facilityDistances[f].distance;
    }

    return sum;

}

/*
Realiza um reset das frequencias de cada facilidade
*/
void resetFacilitiesFreq()
{
    for(int i=0; i<facNumber; i++)
    {
        facilities[i].freq = 0;
    }
}

/*
Retorna a distância entre o cliente e facilidade a qual está conectado no momento
*/
int getCurrentDistance(int custD)
{
    int cFac = customers[custD].currentFacility;

    return getDistance(cFac, custD);
}

/*
Retorna a distância entre o cliente e a segunda facilidade mais póxima a ele
*/
int getNextDistance(int custD)
{
    int cNFac = customers[custD].nextFacility;

    return getDistance(cNFac, custD);
}

/*
Verifica se um movimento satisfaz o critério de aspiração, permitindo que seja executado
mesmo que a facilidade para executá-lo esteja na lista Tabu
*/
bool moveSatisfyAspirationCriteria(int improvement)
{
    if(improvement )
        if(bestCost > (currentCost-improvement))
            return true;

    return false;
}

/*
Retorna a melhor facilidade para ser aberta.
Esta é aquela que gera o melhor ganho para a solução atual.
*/
int getBestOpeningFacility()
{
    int bestImprovement = -(INFINITY);
    int improvement = 0;
    vector<int> bestFacilities;

    /*
    Para cada facilidade que está fechada, itera entre todos os clientes para ver
    se ela gera um ganho na solução
    */
    for(int i=0; i<facNumber; i++)
    {
        if(!isFacilityOpened(i))
        {
            improvement = -getSetUpCost(i);
            for(int j=0; j<custNumber; j++)
            {
                improvement += max(0, getCurrentDistance(j)-getDistance(i, j));
                //O ganho é verificado se a facilidade que pretende ser aberta é mais próxima para um grande número de clientes
            }

            if((!isFacilityTabu(i))||(moveSatisfyAspirationCriteria(improvement)))
            {
                if(improvement > bestImprovement)
                {
                    bestImprovement = improvement;
                    bestFacilities.clear();
                }
                if(improvement == bestImprovement)
                {
                    bestFacilities.push_back(i);
                }
            }
        }
    }
    //Retorna uma facilidade aleatória dentro daquelas definidas como as melhores opções
    int r = rand()%bestFacilities.size();
    return bestFacilities[r];
}

/*
Retorna a melhor facilidade a ser fechada.
Isto é, aquela que gera um menor deterioramento da solução atual.
*/
int getBestClosingFacility()
{
    int bestImprovement = -(INFINITY);
    int improvement = 0;
    vector<int> bestFacilities;

    /*
    Para cada facilidade aberta e que não está na lista Tabu,
    itera entre os clientes da facilidade e verifica a deterioração da solução
    */
    for(int i=0; i<facNumber; i++)
    {
        if(isFacilityOpened(i) && !(isFacilityTabu(i)))
        {
            improvement = getSetUpCost(i);
            facility f = getFacility(i);
            int fConnectionsSize = f.closestConnections.size();
            for(int j=0; j<fConnectionsSize; j++)
            {
                int c = f.closestConnections[j];
                //A deterioração é decidida por aquela facilidade que tem menos clientes bem próximos conectados
                improvement += (getCurrentDistance(c)-getNextDistance(c));
            }

            //Não faz sentido ser menos que -infinito
            improvement = max(-INFINITY, improvement);

            if(improvement >= bestImprovement)
            {
                bestImprovement = improvement;
                bestFacilities.clear();
            }
            if(improvement == bestImprovement)
            {
                bestFacilities.push_back(i);
            }
        }

    }
    //Se nenhuma facilidade foi encontrada, todas as abertas estão na lista Tabu
    if(bestFacilities.size() == 0)
    {
        //Limpa a lista Tabu e procura de novo
        tabuList.clear();
        return getBestClosingFacility();
    }
    else
    {
        //Retorna uma facilidade aleatória dentre as melhores a serem fechadas
        int r = rand()%bestFacilities.size();
        facilities[bestFacilities[r]].freq += 1;
        return bestFacilities[r];
    }
}

/*
Remove uma facilidade da solução atual
*/
void removeFacilityFromSolution(int facD)
{
    int solSize = solution.size();
    for(int i=0; i<solSize; i++)
    {
        if(solution[i]==facD)
        {
            // printf("Removendo facilidade %i\n", facD);
            solution.erase(solution.begin()+i);
            break;
        }
    }
}

/*
Insere uma facilidade na solução atual
*/
void insertFacilityInSolution(int facD)
{
    solution.push_back(facD);
}

/*
Fecha uma facilidade
*/
void closeFacility(int facD)
{
    facilities[facD].isOpened = false;
    removeFacilityFromSolution(facD);
    //Como está sendo fechada, suas conexões devem ser limpas, visto que fechada ela não deve estar conectada a ninguém
    facilities[facD].closestConnections.clear();
    facilities[facD].nextClosestConnections.clear();
}

/*
Disconecta um cliente de uma facilidade
*/
void disconnectCustomerFromFacility(int custD, int facD)
{
    int facConnectionsSize = facilities[facD].closestConnections.size();
    for(int i=0; i<facConnectionsSize; i++)
    {
        //Busca o cliente na lista de conexões da faciliddade
        if(facilities[facD].closestConnections[i] == custD)
        {
            //Apaga o cliente da lista de conexões
            facilities[facD].closestConnections.erase(facilities[facD].closestConnections.begin()+i);
            break;
        }
    }
}

/*
Conecta um cliente a uma facilidade
*/
void connectCustomerToFacility(int custD, int facD)
{
    facilities[facD].closestConnections.push_back(custD);
}

/*
Define a facilidade a qual um cliente está conectado
*/
void setFacilityCustomer(int custD, int facD)
{
    int oldFac = customers[custD].currentFacility;
    customers[custD].currentFacility = facD;

    //Desconecta ele da antiga
    if(oldFac >= 0)
    {
        disconnectCustomerFromFacility(custD, oldFac);
    }
    //Conecta ele a nova
    if(facD >= 0)
    {
        connectCustomerToFacility(custD, facD);
    }


}

/*
Disconecta um cliente da lista de 'segundos mais próximos' de uma facilidade
*/
void disconnectNextCustomerFromFacility(int custD, int facD)
{
    int facNxtConnectionsSize = facilities[facD].nextClosestConnections.size();
    for(int i=0; i<facNxtConnectionsSize; i++)
    {
        if(facilities[facD].nextClosestConnections[i] == custD)
        {
            facilities[facD].nextClosestConnections.erase(facilities[facD].nextClosestConnections.begin()+i);
            break;
        }
    }
}

/*
Conecta um cliente a lista de 'segundos mais próximos' de uma facilidade
*/
void connectNextCustomerToFacility(int custD, int facD)
{
    facilities[facD].nextClosestConnections.push_back(custD);
}

/*
Define a segunda facilidade mais próxima para um cliente
*/
void setNextFacilityCustomer(int custD, int nFacD)
{
    int oldFac =  customers[custD].nextFacility;
    customers[custD].nextFacility = nFacD;
    if(oldFac >= 0)
        disconnectNextCustomerFromFacility(custD, oldFac);
    if(nFacD >= 0)
        connectNextCustomerToFacility(custD, nFacD);
}

/*
Retorna a facilidade atual do cliente
*/
int getCustomerFacility(int custD)
{
    return (customers[custD].currentFacility);
}

/*
Retorna a segunda facilidade mais próxima de um cliente
*/
int getCustomerNextFacility(int custD)
{
    return customers[custD].nextFacility;
}

/*
Retorna a estrutura de uma facilidade a partir de seu índice
*/
facility getFacility(int facD)
{
    return facilities[facD];
}

/*
Retorna a estrutura de um cliente a partir de seu índica
*/
customer getCustomer(int custD)
{
    return customers[custD];
}

/*
Abre uma facilidade
*/
void openFacility(int facD)
{
    facilities[facD].isOpened=true;
    insertFacilityInSolution(facD);
}

/*
Retorna a distância entre uma facilidade e um cliente
*/
int getDistance(int facD, int custD)
{
    if(facD >= 0)
        return customers[custD].facilityDistances[facD].distance;
    else
        return INFINITY;
}

/*
Retorna a facilidade aberta mais próxima a um cliente que não é fac1 ou fac2
*/
int getCloserFacilityWhichIsNot(int custD, int fac1, int fac2, int flag=0)
{
    for(int i=0; i<facNumber; i++)
    {
        if((customers[custD].orderedFacilityDistances[i].facility != fac1)&&(customers[custD].orderedFacilityDistances[i].facility != fac2))
        {
            if(isFacilityOpened(customers[custD].orderedFacilityDistances[i].facility))
            {
                if(flag)
                    flag--;
                else
                {
                    return customers[custD].orderedFacilityDistances[i].facility;
                }

            }
        }
    }
    //Se não haver mais nenhuma aberta, não conecta a nenhuma
    return -1;
}

/*
Fecha ou abre uma facilidade, desconectado/conectando os clientes conforme a ação
*/
int swapFacilities(int facD)
{
    facility F = getFacility(facD);
    int improvement=0;

    if(isFacilityOpened(facD))
    {
        //Facilidade deve ser fechada
        int c;

        //Reconecta todos os clientes da facilidade  a ser fechada para a próxima mais perto deles e que está aberta
        int fConnectionsSize = F.closestConnections.size();
        for(int i=0; i<fConnectionsSize; i++)
        {
            c=F.closestConnections[i];
            improvement += (getCurrentDistance(c)-getNextDistance(c));
            setFacilityCustomer(c, getCustomerNextFacility(c));
            int nF = getCloserFacilityWhichIsNot(c, facD, getCustomerFacility(c));
            setNextFacilityCustomer(c, nF);
        }
        int fNxtConnectionsSize = F.nextClosestConnections.size();
        //Atualiza a segunda facilidade mais próxima para os 'segundos mais próximos' a facilidade que está sendo fechada
        for(int i=0; i<fNxtConnectionsSize; i++)
        {
            c=F.nextClosestConnections[i];
            int nnF = getCloserFacilityWhichIsNot(c, facD, getCustomerFacility(c));
            setNextFacilityCustomer(c, nnF);
        }

        //Fecha de fato a facilidade
        closeFacility(facD);
    }
    else
    {
        //Facilidade deve ser aberta
        openFacility(facD);
        int distance;
        //Verifica se ela é a mais próxima para algum cliente
        for(int i=0; i<custNumber; i++)
        {
            distance = max(0, (getCurrentDistance(i)-getDistance(facD, i)));

            //Se for, define como a facilidade atual para o cliente
            if(distance > 0)
            {
                improvement += distance;
                setNextFacilityCustomer(i, getCustomerFacility(i));
                setFacilityCustomer(i, facD);
            }
            else if(getCustomer(i).nextFacility == -1)
            {
                setNextFacilityCustomer(i, facD);
            }
            //Se não, verifica se é a segunda mais próxima para o cliente
            else if( max(0, getNextDistance(i)-getDistance(facD, i)) > 0)
            {
                setNextFacilityCustomer(i, facD);
            }
        }
    }

    //Retorna o ganho (que e negativo em caso de fechamento) da nova solução
    return improvement;
}

/*
Adiciona uma facilidade a lista tabu
*/
void addToTabuList(int facD)
{
    tabuList.push_back(facD);
}

/*
Verifica se a lista tabu não ultrapassou seu tamanho limite.
Se sim, apaga on n primeiros elementos, onde n é o excesso.
*/
void checkTabuListSize()
{
    int actTabuSize = tabuList.size();
    if( actTabuSize > tabuListSize)
    {
        int n = actTabuSize-tabuListSize;
        tabuList.erase(tabuList.begin(), tabuList.begin()+n);
    }
}

/*
Obtém o número de trocas de facilidades a serem executadas.
É influenciado pelo numero de iterações sem sucesso que se passaram (iterationCount)
*/
int getSwapCount()
{
    return max(1, min(iterationCount*swapModifier, P-2));
}

/*
Faz a inicialização das variáveis e ambiente para cálculo do problema
*/
void initializeProblem(FILE *inputFile, int fileType)
{
    if(fileType==1)
        pmedFiles(inputFile);
    else if(fileType==2)
        gapFiles(inputFile);

    tabuList.clear();

    swapCount = getSwapCount();

    generateInitialSolution();

    currentCost = totalCost();

    bestCost = currentCost;

    tabuListSize = tabuSizeFactor*P;

    bestSolution = solution;
}


/*
Função para retornar a melhor solução encontrada anteriormente (evitando ficar preso a caminhos de ótimo local)
*/
void rollBackSolution()
{
    for(int i=0; i<solution.size(); i=0)
        closeFacility(solution[i]);

    int solSize = bestSolution.size();
    for(int i=0; i<solSize; i++)
        openFacility(bestSolution[i]);

    solution = bestSolution;

    for(int i=0; i<custNumber; i++)
    {
        fdTuple closer = getCloserOpenedFacility(customers[i]);
        fdTuple secondCloser = getCloserOpenedFacility(customers[i], 1);

        customers[i].currentFacility = closer.facility;
        customers[i].nextFacility = secondCloser.facility;

        if(closer.facility >= 0)
            facilities[closer.facility].closestConnections.push_back(i);
        if(secondCloser.facility >= 0)
            facilities[secondCloser.facility].nextClosestConnections.push_back(i);
    }

    resetFacilitiesFreq();

}

bool isLimitTime(double currentTime)
{
    if(tmlim == 0)
        return false;

    return currentTime >= tmlim;
}

bool stopCriteriaSatisfied(int iterC, int maxR, float diffTime, int mstpc)
{
    if(mstpc == 1)
    {
        return iterC >= maxR;
    }
    else if(mstpc == 2)
    {
        return isLimitTime(diffTime);
    }
    else
    {
        return (iterC >= maxR || isLimitTime(diffTime));
    }

}

/*
A função principal do módulo, que faz chamadas para as outras e calcula de fato o problema
*/
int tabuSearchPMedian(FILE *inputFile, int fileType, int maxR, int maxD, int swapM, float tabuFac, int tlim, int mstpc)
{
    srand ( time(NULL) );

    time_t start_time;
    time_t current_time;

    double elapsed_time=0;
    double ee = 0;
    int flag=0;
   // vector<pair<int, float> > data;

    maxRuns = maxR;
    maxDepth = maxD;
    swapModifier = swapM;
    tabuSizeFactor = tabuFac;
    opFile = fileType;
    tmlim = tlim;

   // vector<pair<int,int> > bests;
   // vector<customer> cc;

    initializeProblem(inputFile, fileType);

    vector<int> inSol = solution;
    printf("Custo da solucao inicial:  %i\nSolucao inicial:    ", currentCost);
    for(int i=0; i<solution.size(); i++)
    {
        printf("%i, ", solution[i]+1);
    }

    printf("\n\n");

    start_time = time (NULL);
    current_time = start_time;

   // data.push_back(make_pair(bestCost, difftime(current_time, start_time)));

    while (!stopCriteriaSatisfied(iterationCount, maxRuns, difftime(current_time, start_time), mstpc))
    {
        //Fechar facilidades
        for(int i=0; i<swapCount; i++)
        {
            int f = getBestClosingFacility();
            int g = swapFacilities(f); //Sempre retorna negativo para o fechamento
            currentCost = currentCost-(g);
        }
        //Abrir facilidades
        for(int i=0; i<swapCount; i++)
        {
            int f = getBestOpeningFacility();
            int g = swapFacilities(f);  //sempre retorna positivo ou 0 para abertura
            currentCost = currentCost-(g);
            addToTabuList(f);
            checkTabuListSize();
        }

        /*time(&current_time);
        if(difftime(current_time, start_time)-ee >= 1)
        {
            ee=difftime(current_time, start_time);
            //data.push_back(make_pair(bestCost, ee));
        }*/

        if(currentCost < bestCost)
        {
            bestCost = currentCost;
            iterationCount = 0;
            iterationDepth = 0;
            bestSolution = solution;
        }
        else
        {
            if(maxDepth == 0)
                iterationCount++;
            else
            {
                iterationDepth++;
                if(iterationDepth == maxDepth)
                {
                    iterationDepth=0;
                    iterationCount++;
                    rollBackSolution();
                    currentCost = bestCost;
                }
            }
        }
        current_time = time(NULL);
        if(difftime(current_time, start_time) >= elapsed_time+10)
        {
            elapsed_time = difftime(current_time, start_time);
            printf("*:    Melhor valor:  %i  |  tempo:  %.2f\n", bestCost, elapsed_time);
        }
        swapCount = getSwapCount();
    }
    if(currentCost != bestCost)
        rollBackSolution();


    sort(solution.begin(), solution.end());

    printSolution();

    /*FILE *dataFile = fopen("data.dat", "wt");

    for(int i=0; i<data.size(); i++)
    {
        fprintf(dataFile, "%i\t\t%f\n", data[i].first, data[i].second);
    }

    fclose(dataFile);*/

    return totalCost();
}
