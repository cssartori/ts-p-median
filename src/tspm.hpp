#include <cstdio>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <ctime>


typedef struct fdTuple
{
    int facility;
    int distance;
}fdTuple;

typedef struct facility
{
    int ID;
    int freq;
    bool isOpened;
    std::vector<int> closestConnections;
    std::vector<int> nextClosestConnections;
} facility;

typedef struct customer
{
    int ID;
    int currentFacility;
    int nextFacility;
    std::vector<fdTuple> facilityDistances;
    std::vector<fdTuple> orderedFacilityDistances;
} customer;


const int INFINITY = 1000000;


void printCustomer(customer C);
bool compDistances (fdTuple tOne, fdTuple tTwo);
int max(int n1, int n2);
int min(int n1, int n2);
bool isFacilityOpened(int facD);
bool isFacilityTabu(int facD);
fdTuple getCloserOpenedFacility(customer C, int startIndex);
void pmedFiles(FILE *inputFile);
void printCustomer(customer C);
void generateInitialSolution();
int totalCost();
void closeFacility(int facD);
void initializeProblem(FILE *inputFile, int fileType);
int tabuSearchPMedian(FILE *inputFile, int fileType, int maxR, int maxD, int swapM, float tabuFac, int tmlim=0, int mstpc=1);

int swapFacilities(int facD);
bool moveSatisfyAspirationCriteria(int improvement);

void openFacility(int facD);
void closeFacility(int facD);

int getDistance(int facD, int custD);
customer getCustomer(int custD);
facility getFacility(int facD);
int getCustomerNextFacility(int custD);
int getCustomerFacility(int custD);
int getBestClosingFacility();
int getBestOpeningFacility();
int getNextDistance(int custD);

void setNextFacilityCustomer(int custD, int nFacD);
void setFacilityCustomer(int custD, int facD);

void checkInfinityBound(int *value);

void addToTabuList(int facD);
void checkTabuListSize();
