#include "tspm.hpp"

#include<string>

using namespace std;

void errorUsage()
{
        fprintf(stderr, "\nBusca Tabu para P-medianas: **Erro em comando.\n\n"
        "  -f        Arquivo de dados para leitura (obrigatorio).\n"
        "  -o        Tipo do arquivo de dados: pmed = 1, GAP = 2 (obrigatorio).\n"
        "  -mruns    Numero maximo de tentativas novas quando nao ocorre melhora na\n            resposta (padrao = 15).\n"
        "  -mdepth   Numero maximo de iteracoes sem melhora que serao executadas antes \n            de se retornar para a melhor solucao encontrada ate o momento \n            (padrao = 10).\n"
        "  -swapm    Modificador do numero de trocas de facilidades realizado por \n            iteracao (padrao = 4).\n"
        "  -tabufac  Fator multiplicador para o tamanho da lista tabu: P*tabuFactor \n            (padrao = 0.75).\n"
        "  -tmlim    Tempo limite de execução em segundos (padrao = sem limite).\n"
        "  -mstpc    Criterio maximo de parada (programa so para ao atingir ele):\n            Max. Iteracoes = 1, Max. Tempo = 2, Primeiro a ser atingido = 3\n            (padrao = 3).\n"
        );
        exit(-1);
}


int main(int argc, char **argv)
{
    time_t start_time;
    time_t end_time;


    int opFile = -1;
    int maxRuns = 15, maxDepth = 10, swapModifier = 4;
    int mstpc = 3;
    float tabuSizeFactor = 0.75;
    int tmlim=0;
    string fileName = "";
    string command;

    if(argc < 2)
        errorUsage();

    for (int i = 1; i < argc; i++){
        if (argv[i][0] != '-') errorUsage();
        command = argv[i];
        i++;

        if(command == "-mruns")
            maxRuns = atoi(argv[i]);

        else if(command == "-mdepth")
            maxDepth = atoi(argv[i]);

        else if(command == "-swapm")
            swapModifier = atoi(argv[i]);

        else if(command == "-tabufac")
            tabuSizeFactor = atof(argv[i]);

        else if(command =="-tmlim")
            tmlim = atoi(argv[i]);

        else if(command =="-mstpc")
            mstpc = atoi(argv[i]);

        else if(command == "-f")
            fileName = argv[i];

        else if(command == "-o")
            opFile = atoi(argv[i]);

        else
            errorUsage();

    }

    if(tmlim == 0 && mstpc == 2)
    {
        printf("\n*********AVISO: Criterio de parada maximo eh tempo, mas nenhum tempo limite foi\n\t        dado. Assumindo criterio de parada como padrao = 3.\n\n");
        mstpc = 3;
    }

    FILE *inputFile = fopen(fileName.c_str(), "rt");
    if(inputFile == NULL)
    {
        printf("Erro ao abrir arquivo.\n");
        exit(-1);
    }

    start_time = time(NULL);
    printf("============================= Iniciada a Busca Tabu ============================\n\n\n");

    int value = tabuSearchPMedian(inputFile, opFile, maxRuns, maxDepth, swapModifier, tabuSizeFactor, tmlim, mstpc);

    end_time = time(NULL);

    printf("\nCUSTO DA MELHOR SOLUCAO ENCONTRADA:    %i\n\n", value);

    printf("TEMPO DE RESOLUCAO:                    %.4f  segundos\n", difftime (end_time,start_time));
    printf("\n\n=============================== Fim da Busca Tabu ==============================\n\n");

}

