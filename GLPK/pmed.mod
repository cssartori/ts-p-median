set M; /*Clientes*/

set F; /*Facilidades*/

param INFINITY;

param custoTransporte {i in M, j in F}; /*Custo de transporte da facilidade j para o cliente i*/

param p; /*Numero p de facilidades a serem instaladas*/

var x{j in F} binary; /*1 - Facilidade j instalada 
						0 - c.c. */

var y{i in M, j in F} binary; /*1 - Cliente i suprido por Facilidade j
								0 - c.c. */
						 
minimize custos: (sum{i in M}(sum{j in F}(y[i,j]*custoTransporte[i,j])));


s.t. exatasFacilidades: sum{j in F}(x[j]) = p; 		/*Garante que exatas p facilidades foram instaladas*/
s.t. limFporC{i in M}: sum{j in F}(y[i, j]) = 1; 	/*Um cliente só pode ser suprido por uma facilidade*/ 
s.t. minCporF{j in F, i in M}: (y[i, j]) <= (x[j]); 	/*Uma facilidade só pode atender se tiver sido instalada*/


end;
